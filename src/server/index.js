const express = require('express');
const path = require('path');

let app=express()

app.get('/*', function(req, res) {
    if (process.argv[2] === 'dev') {
        res.sendFile(path.join(__dirname, 'public', 'index.html'));
    } else {
        res.sendFile(path.join(__dirname, 'build', 'index.html'));
    }
});

// app.use(express.static('dist'));
app.listen(process.env.PORT || 8000, () => console.log(`Listening on port ${process.env.PORT || 8000}!`));
